// Include gulp
var gulp = require('gulp');

// Include plugins
var jshint       = require('gulp-jshint');
var sass         = require('gulp-sass');
var concat       = require('gulp-concat');
var uglify       = require('gulp-uglify');
var livereload   = require('gulp-livereload');
var connect      = require('gulp-connect');
var plumber      = require('gulp-plumber');
var postcss      = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var rename       = require('gulp-rename');
var replace      = require('gulp-replace');
var cssmin       = require('gulp-cssmin');


// Live Reload
gulp.task('connect', function() {
    connect.server({
        livereload: true
    });
});

// JS files
gulp.task('js', function() {
    gulp.src('./js/**/*.js')
        .pipe(livereload());
});

// HTML files
gulp.task('html', function() {
    gulp.src('./*.html')
        .pipe(livereload());
});

// Compile Sass
gulp.task('sass', function() {
    return gulp.src('css/sass/*.scss')
        .pipe(sass())
        .pipe(postcss([ autoprefixer() ]))
        .pipe(gulp.dest('./'))
        .pipe(livereload());
});

// Concat and Uglify Vendor JS Libs
// - place vendor files in 'js/libs/*.js'
// - in gulp.src order the files as you would like them to appear in your vendor file
gulp.task('ugly', function() {
    return gulp.src([
            './js/libs/jquery.min.js',
            './js/isotope.pkgd.min.js'
        ])
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./js/'));
});

// Watch files for changes
// Uncomment when building without Marketo
gulp.task('watch', function() {
    livereload.listen();
    gulp.watch('js/*.js', ['js']);
    gulp.watch('css/sass/**/*.scss', ['sass']);
    gulp.watch('./*.html', ['html']);
    gulp.watch('./style.css');
});

gulp.task('dist-min', function() {
    return gulp.src(['./style.css', './*.html'])
        .pipe(cssmin())
        .pipe(gulp.dest('./dist/min/'));
});

// No min for Amazon
gulp.task('dist', function() {
    return gulp.src(['./style.css', './forms.css', './index.html'])
        .pipe(gulp.dest('./dist'));
});


// Default Task
gulp.task('default', ['sass', 'watch', 'connect']);




